# fix the issue of maximum recursion limits exceeded
import sys, threading
sys.setrecursionlimit(800000)
threading.stack_size(67108864)


def alg(input_path):
    graph = {}
    g_rev = {}
    n = 0
    
    with open(input_path) as f:
        for line in f:
            k, v = list(map(int, line.rstrip().split(' ')))
            if max(k, v) > n:
                n = max(k, v)
    
            if k in graph:
                graph[k].append(v)
            else:
                graph[k] = [v]

            if v in g_rev:
                g_rev[v].append(k)
            else:
                g_rev[v] = [k]

    cnt_scc = kosaraju_2_pass(graph, g_rev, n)
    cnt_scc = sorted(cnt_scc.values(), reverse = True)
    
    if len(cnt_scc) > 5:
        cnt_scc = cnt_scc[:5]
    else:
        cnt_scc.extend([0] * (5 - len(cnt_scc)))

    result = ','.join(map(str, cnt_scc))

    return(result)

def kosaraju_2_pass(graph, g_rev, n):

    finishing_time, _, _ = dfs_loop(g_rev, n)

    graph = {finishing_time[k-1]: [finishing_time[v0-1] for v0 in v] for k, v in graph.items()}
    _, leader, cnt_scc = dfs_loop(graph, n)
    
    return(cnt_scc)


def dfs_loop(graph, n):

    t = 0        # global variable for finishing time
    s = None     # global variable for source vertex
    cnt_scc = {} # global variable for number of vertices in each scc

    explored = [0] * n # keep track of nodes explored
    leader = [0] * n
    finishing_time = [0] * n

    def dfs(graph, source):
        nonlocal t
        nonlocal s
        nonlocal cnt_scc
        nonlocal explored
        nonlocal leader
        nonlocal finishing_time

        # set source vertex to explored
        explored[source - 1] = 1

        # set leader to source
        leader[source - 1] = s
        if s in cnt_scc:
            cnt_scc[s] += 1
        else:
            cnt_scc[s] = 1

        # search through the edges
        if source in graph:
            for j in graph[source]:
                if not explored[j - 1]:
                    dfs(graph, j)

        t += 1
        finishing_time[source - 1] = t

    for i in range(n-1, -1, -1):
        if not explored[i]:
            s = i + 1
            dfs(graph, i + 1)

    
    #print (f'explored: {explored}')
    #print (f'leader: {leader}')
    #print (f'finishing_time: {finishing_time}')

    return((finishing_time, leader, cnt_scc))


def main():

    result = alg('/home/rachel/Documents/algorithms/2_graph_search/SCC.txt')
    print(result)
    return result


if __name__ == '__main__':
    thread = threading.Thread(target=main)
    thread.start()

