import heapq

def alg(input_path):
    graph = {}

    with open(input_path) as f:
        for line in f:
           line = line.rstrip().split('\t')
           # edge is format (weight, beginning node, ending node)
           edges = [(int(x.split(',')[1]), int(x.split(',')[0])) for x in line[1:]]
           graph[int(line[0])] = edges
    #print(graph[1])

    shortest = dijkstra(graph, 200)
    
    result_list = [6,36,58,81,98,114,132,164,187,196] 
    result = [shortest[x] for x in result_list]

    return(','.join(map(str, result)))

def dijkstra(graph, n):
    #n = len(graph) # number of nodes
    explored = [0] * n
    shortest = [1000000] * n

    # initialization: node 1
    explored[0] = 1
    shortest[0] = 0
    v_x = graph[1] # edges from node 1
    heapq.heapify(v_x)

    while sum(explored) < n:
        # pick the node that minimizes greedy score
        while v_x:
            A_w, w_star = heapq.heappop(v_x)
            if not explored[w_star - 1]:
                break

        shortest[w_star-1] = A_w

        # update frontier and heap
        explored[w_star-1] = 1
        #for i, edge in enumerate(v_x):
        #    if edge[1] == w_star:
        #        v_x[i] = v_x[-1]
        #        _ = v_x.pop()
        #        heapq.heapify(v_x)

        if w_star in graph:
            for edge in graph[w_star]:
                if not explored[edge[1]-1]:
                    A_w2 = A_w + edge[0]
                    heapq.heappush(v_x, (A_w2, edge[1]))

        if not v_x:
            break

    return (shortest)


if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/2_graph_search/dijkstraData.txt')
    #graph = {1:[(1,2),(4,3)],2:[(2,3),(6,4)],3:[(3,4)]}
    #result = dijkstra(graph, 4)
    print(result)

