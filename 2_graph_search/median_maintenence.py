import heapq

class MinHeap(object):
    def __init__(self):
        self.h = []

    def heappush(self, x):
        heapq.heappush(self.h, x)

    def heappop(self):
        return heapq.heappop(self.h)

    def __getitem__(self, i):
        return self.h[i]

    def __len__(self):
        return len(self.h)

class MaxHeap(MinHeap):
    def heappush(self, x):
        heapq.heappush(self.h, -x)

    def heappop(self):
        return -heapq.heappop(self.h)

    def __getitem__(self, i):
        return -self.h[i]

def alg(input_path):
    h_low = MaxHeap()
    h_high = MinHeap()
    cnt = 0
    median = []

    with open(input_path) as f:
        for line in f:
            cnt += 1
            if cnt == 1:
                h_low.heappush(int(line))
            
            elif cnt % 2:
                if int(line) <= h_low[0]:
                    h_low.heappush(int(line))
                else:
                    h_high.heappush(int(line))
                    h_low.heappush(h_high.heappop())

            else:
                if int(line) <= h_low[0]:
                    if len(h_low) < len(h_high):
                        h_low.heappush(int(line))
                    elif len(h_low) > len(h_high):
                        h_high.heappush(h_low.heappop())
                        h_low.heappush(int(line))

                else: 
                    if len(h_low) > len(h_high):
                        h_high.heappush(int(line))
                    elif len(h_low) < len(h_high):
                        h_low.heappush(h_high.heappop())
                        h_high.heappush(int(line))


            median.append(h_low[0])

    result = sum(median) % 10000

    return result 

if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/2_graph_search/Median.txt')
    print(result)
