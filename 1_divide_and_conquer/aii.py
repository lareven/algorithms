def alg(input_path):
    with open(input_path, 'r') as f:
        num_array = f.read().splitlines()
        num_array = list(map(int, num_array))
    
    result = aii(num_array)

    return (result)

def aii(num_array, pad = 0):
    len_arr = len(num_array)

    if len_arr == 1:
        if num_array[0] == pad:
            return 'true'
        else:
            return 'false'

    else:
        mid_id = len_arr // 2
        
        if num_array[mid_id - 1] == mid_id - 1 + pad:
            return 'true'
        elif num_array[mid_id - 1] < mid_id - 1 + pad:
            result = aii(num_array[mid_id:], pad = mid_id+pad)
        else:
            result = aii(num_array[:mid_id])

        return (result)
    
if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/stanford-algs-master/testCases/course1/optionalTheoryProblemsBatch1/question3/input_Rahmeen14_02.txt')
    print(result)
