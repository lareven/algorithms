import math
import random

def alg(input_path):
    with open(input_path, 'r') as f:
        num_array = f.read()
        num_array = list(map(int, num_array.split(' ')))
    
    result, num_array = rselect(num_array, len(num_array)-1)

    return (result)

def rselect(num_array, order_stat):
    arr_len = len(num_array)

    if arr_len == 1:
        return (num_array[0], num_array)
    else:
        # randomly choose pivot
        p_id = random.randint(0, arr_len - 1)
        #print(p_id)

        # swap pivot to first element
        num_array[0], num_array[p_id] = num_array[p_id], num_array[0]

        # partition
        num_array, p_new_id = partition(num_array, 0, arr_len)
        #print(num_array)
        #print(p_new_id)

        # recursive calls - one side only
        if p_new_id == order_stat - 1:
            return (num_array[p_new_id], num_array)

        elif p_new_id < order_stat - 1:
            result, num_array[(p_new_id+1):arr_len] = rselect(num_array[(p_new_id+1):arr_len], order_stat - p_new_id - 1)

        elif p_new_id > order_stat - 1:
            result, num_array[0:p_new_id] = rselect(num_array[0:p_new_id], order_stat)

        return (result, num_array) 

def partition(num_array, left, right):
    pivot = num_array[left]
    i = left + 1
    for j in range(left+1, right):
        if num_array[j] < pivot:
            num_array[j], num_array[i] = num_array[i], num_array[j]
            i += 1
    
    num_array[left], num_array[i-1] = num_array[i-1], num_array[left]

    return(num_array, i-1)

if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/stanford-algs-master/testCases/course1/optionalTheoryProblemsBatch1/question1/input_Rahmeen14_01.txt')
    #result = rselect([3,2,4,6,35,9,0],2)
    print(result)

