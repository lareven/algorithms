def alg(input_path):
    with open(input_path, 'r') as f:
        num_array = f.read().splitlines()
        num_array = list(map(int, num_array))
    
    result = unimodal(num_array)

    return (result)

def unimodal(num_array):
    len_arr = len(num_array)

    if len_arr == 1:
        return num_array[0]

    else:
        mid_id = len_arr // 2
        
        if num_array[mid_id - 1] < num_array[mid_id]:
            max_ele = unimodal(num_array[mid_id:])
        else:
            max_ele = unimodal(num_array[:mid_id])

        return (max_ele)
    
if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/stanford-algs-master/testCases/course1/optionalTheoryProblemsBatch1/question2/input_Rahmeen14_01.txt')
    print(result)
