import numpy as np

def alg(input_path):
    #with open(input_path, 'r') as f:
    #    num_array = f.read().splitlines()
    #    num_array = list(map(int, num_array))
    
    num_array = np.loadtxt(input_path)
    result = local_min(num_array)
    #result = compare_edge(num_array, 2,1)
    return (result)

def local_min(num_array):
    size = num_array.shape[0]

    if size == 1:
        return (num_array[0, 0],0,0)

    else:
        mid_id = size // 2
        
        l_min, i, j = local_min(num_array[:mid_id, :mid_id])
        if compare_edge(num_array, i, j):
            print(i,j)
            return(l_min,i,j)
        
        l_min, i, j = local_min(num_array[:mid_id, mid_id:])
        if compare_edge(num_array, i, j):
            print(i,j)
            return(l_min,i,j)
        
        l_min, i, j = local_min(num_array[mid_id:, :mid_id])
        if compare_edge(num_array, i, j):
            print(i,j)
            return(l_min,i,j)
        
        l_min, i, j = local_min(num_array[mid_id:, mid_id:])
        if compare_edge(num_array, i, j):
            print(i,j)
            return(l_min,i,j)
        
        #return (0,0,0)
    
def compare_edge(num_array, i, j):
    print(num_array)
    size = num_array.shape[0]
    mid_id = size // 2
    print(mid_id)

    is_min = True

    if (i == mid_id - 1) | (j == mid_id - 1):
        if (num_array[i,j] > num_array[i-1,j]) | (num_array[i,j] > num_array[i+1,j]) | (num_array[i,j] > num_array[i,j-1]) | (num_array[i,j] > num_array[i,j+1]):
            is_min = False

    return is_min


if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/stanford-algs-master/testCases/course1/optionalTheoryProblemsBatch1/question4/input_Rahmeen14_02.txt')
    print(result)
