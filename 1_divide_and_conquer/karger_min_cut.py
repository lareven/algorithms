import math
import random
import copy

def alg(input_path):
    with open(input_path, 'r') as f:
        graph = f.read().splitlines()
        for i in range(len(graph)):
            graph[i] = graph[i].strip().split('\t')
            #graph[i] = list(map(int, graph[i].split(' ')))
    #print(graph)

    result = karger_min_cut(graph)
    return (result)

def karger_min_cut(graph):
    num_vertices = len(graph)
    min_edges = sum([len(x) - 1 for x in graph])/2

    for k in range(num_vertices**2):
        new_graph = copy.deepcopy(graph)
        num_new_vertices = len(new_graph)
        
        while num_new_vertices > 2:
            new_graph = contract_edge(new_graph)
            num_edges = sum([len(x) - 1 for x in new_graph])/2
            num_new_vertices = len(new_graph)
            #print(new_graph)

        if num_edges < min_edges:
            min_edges = num_edges

    return (int(min_edges))

def contract_edge(graph):
    id_vertex1 = random.randint(0, len(graph) - 1)
    vertex1 = graph[id_vertex1][0]

    id_v_vertex2 = random.randint(1, len(graph[id_vertex1]) - 1)
    vertex2 = graph[id_vertex1][id_v_vertex2]

    vertices = [x[0] for x in graph]
    id_vertex2 = vertices.index(vertex2)

    #graph[id_vertex2][:] = [x for x in graph[id_vertex2] if x != vertex1]
    #graph[id_vertex1].remove(vertex2)

    #print(vertex1, vertex2)

    for v in graph[id_vertex2][1:]:
        v_id = vertices.index(v)
        graph[v_id] = [vertex1 if x == vertex2 else x for x in graph[v_id]]

    graph[id_vertex1].extend(graph[id_vertex2][1:])
    graph[id_vertex1][1:] = [x for x in graph[id_vertex1][1:] if x!= vertex1 and x != vertex2]
    graph.remove(graph[id_vertex2])

    return (graph)

if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/1_Divide_and_conquer/kargerMinCut.txt')
    #result = alg('/home/rachel/Documents/algorithms/stanford-algs-master/testCases/course1/assignment4MinCut/input_random_1_6.txt')
    #result = karger_min_cut([[1,2,4],[2,1,3],[3,2,4],[4,1,3]])
    print(result)

