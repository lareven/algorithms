import math

def alg(input_path):
    with open(input_path, 'r') as f:
        num_array = f.read().splitlines()
        num_array = list(map(int, num_array))
    arr1, arr2, arr3 = num_array.copy(), num_array.copy(), num_array.copy()

    count1, sorted_array = quick_sort(arr1, 'first')
    count2, sorted_array = quick_sort(arr2, 'last')
    count3, sorted_array = quick_sort(arr3, 'median')
    return ([count1, count2, count3])

def quick_sort(num_array, type_pivot, count = 0):
    arr_len = len(num_array)
    count += arr_len - 1

    if arr_len == 1:
        return (count, num_array)
    else:
        # swap pivot to first element in the array
        if type_pivot == 'last': # do nothing if first element is the pivot
            num_array[0], num_array[arr_len - 1] = num_array[arr_len - 1], num_array[0]

        elif type_pivot == 'median':
            mid_element = math.ceil(arr_len/2)
            comp = [num_array[0],num_array[mid_element-1],num_array[arr_len-1]]

            if (comp[1] < max(comp)) & (comp[1] > min(comp)):
                num_array[0], num_array[mid_element - 1] = num_array[mid_element - 1], num_array[0]
            elif (comp[2] < max(comp)) & (comp[2] > min(comp)):
                num_array[0], num_array[arr_len - 1] = num_array[arr_len - 1], num_array[0]

        # partition
        num_array, i = partition(num_array, 0, arr_len)

        # recursive calls
        if i > 1:
            count, num_array[left:(i-1)] = quick_sort(num_array[left:(i-1)], type_pivot, count)

        if i < right:
            count, num_array[i:right] = quick_sort(num_array[i:right], type_pivot, count)

        return (count, num_array) 

def partition(num_array, left, right):
    pivot = num_array[left]
    i = left + 1
    for j in range(left+1, right):
        if num_array[j] < pivot:
            num_array[j], num_array[i] = num_array[i], num_array[j]
            i += 1
    
    num_array[left], num_array[i-1] = num_array[i-1], num_array[left]

    return(num_array, i)

if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/1_Divide_and_conquer/QuickSort.txt')
    print(result)

