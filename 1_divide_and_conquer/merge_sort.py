def my_merge_sort(my_array):
    length = len(my_array)
    
    if length == 1:
        return my_array
    else:
        mid_point = int(length / 2)

        sub_array1 = [None] * mid_point
        sub_array2 = [None] * (length - mid_point)
        for i in range(length):
            if i < mid_point:
                sub_array1[i] = my_array[i]
            else:
                sub_array2[i - mid_point] = my_array[i]

        sub_array1 = my_merge_sort(sub_array1)
        sub_array2 = my_merge_sort(sub_array2)
        
        merge_array = [None] * length
        i = 0
        j = 0
        for k in range(length):
            if (i < len(sub_array1)) & (j < len(sub_array2)):
                if sub_array1[i] < sub_array2[j]:
                    merge_array[k] = sub_array1[i]
                    i += 1

                elif sub_array1[i] > sub_array2[j]:
                    merge_array[k] = sub_array2[j]
                    j += 1

                else:
                    merge_array[k] = sub_array1[i]
                    merge_array[k+1] = sub_array1[i]
                    i += 1
                    j += 1
                                       
            elif i == len(sub_array1):
                merge_array[k] = sub_array2[j]
                j += 1
                
            elif j == len(sub_array2):
                merge_array[k] = sub_array1[i]
                i += 1
                
    return merge_array               

if __name__ == '__main__':
    print(my_merge_sort([3,1,5,7,4]))