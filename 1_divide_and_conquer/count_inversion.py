def alg(input_path):
    with open(input_path, 'r') as f:
        num_array = f.read().splitlines()
        num_array = list(map(int, num_array))

    count, sorted_array = count_inversion(num_array)

    return (count)

def count_inversion(num_array):
    arr_len = len(num_array)
    if arr_len == 1:
        return (0, num_array)
    else:
        mid_point = arr_len // 2
        array1 = num_array[:mid_point]
        array2 = num_array[mid_point:]

        count1, array1 = count_inversion(array1)
        count2, array2 = count_inversion(array2)

        i = 0
        j = 0
        count3 = 0
        for k in range(arr_len):
            if (i < len(array1)) & (j < len(array2)):
                if array1[i] < array2[j]:
                    num_array[k] = array1[i]
                    i += 1
                elif array1[i] > array2[j]:
                    num_array[k] = array2[j]
                    j += 1
                    count3 += (mid_point - i)

            elif i == len(array1):
                num_array[k] = array2[j]
                j += 1

            elif j == len(array2):
                num_array[k] = array1[i]
                i += 1

        return (count1 + count2 + count3, num_array) 


if __name__ == '__main__':
    result = alg('/home/rachel/Documents/algorithms/1_Divide_and_conquer/IntegerArray.txt')
    print(result)
